/*
  Copyright 2019 Esri
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { loadModules } from 'esri-loader';
import esri = __esri; // Esri TypeScript Types
import { map } from 'leaflet';

@Component({
  selector: 'app-esri-map',
  templateUrl: './esri-map.component.html',
  styleUrls: ['./esri-map.component.css']
})
export class EsriMapComponent implements OnInit {

  @Output() mapLoadedEvent = new EventEmitter<boolean>();

  // The <div> where we will place the map
  @ViewChild('mapViewNode') private mapViewEl: ElementRef;
  @ViewChild('infoDiv') private infoDiv: ElementRef;

  /**
   * _zoom sets map zoom
   * _center sets map center
   * _basemap sets type of map
   * _loaded provides map loaded status
   */
  private paramZoom = 13;
  private paramCenter: Array<number> = [-6.882691, 107.610764];
  private paramBasemap = 'gray';
  private paramIsloaded = false;
  private paramGround = 'world-elevation';

  private EsriMap: any;
  private EsriMapView: any;
  private EsriSceneView: any;
  private EsriLocator: any;
  private EsriExpand: any;
  private EsriBasemapToggle: any;
  private EsriBasemapGallery: any;
  private EsriFeatureLayer: any;
  private EsriNavigationToggle: any;
  private EsriPoint: any;
  private EsriPolygon: any;
  private EsriLegend: any;
  private EsriConfig: any;
  private EsriRequest: any;
  private EsriGraphicsLayer: any;
  private EsriGraphic: any;

  private maps: any;
  private mapView: any;
  private locatorTask: any;
  private layer: any;
  private legend: any;

  // public varFields = [];
  // public varTemplate = {};
  // public varQuakesRenderer = {};

  public varFields = [
    {
      name: 'ObjectID',
      alias: 'ObjectID',
      type: 'oid'
    }, {
      name: 'id',
      alias: 'id',
      type: 'integer'
    }, {
      name: 'index',
      alias: 'index',
      type: 'double'
    }, {
      name: 'temperature',
      alias: 'temperature',
      type: 'integer'
    }, {
      name: 'place',
      alias: 'place',
      type: 'string'
    }, {
      name: 'time',
      alias: 'time',
      type: 'date'
    }, {
      name: 'url',
      alias: 'url',
      type: 'string'
    }, {
      name: 'types',
      alias: 'types',
      type: 'string'
    }, {
      name: 'type',
      alias: 'type',
      type: 'string'
    }, {
      name: 'title',
      alias: 'title',
      type: 'string'
    }];

  public varTemplate = {
    title: '{title}',
    content: [{
      type: 'fields',
      fieldInfos: [{
        fieldName: 'id',
        label: 'id',
        visible: true
      }, {
        fieldName: 'type',
        label: 'type',
        visible: true
      }, {
        fieldName: 'index',
        label: 'index',
        visible: true
      }, {
        fieldName: 'temperature',
        label: 'temperature',
        visible: true
      }, {
        fieldName: 'place',
        label: 'place',
        visible: true
      }, {
        fieldName: 'time',
        label: 'time',
        visible: true,
        format: {
          dateFormat: 'short-date-short-time'
        }
      }, {
        fieldName: 'url',
        label: 'url',
        visible: true
      }, {
        fieldName: 'types',
        label: 'types',
        visible: true
      }]
    }]
  };

  public varQuakesRenderer = {
    type: 'simple', // autocasts as new SimpleRenderer()
    symbol: {
      type: 'simple-marker', // autocasts as new SimpleMarkerSymbol()
      style: 'circle',
      size: 20,
      color: [255, 0, 85, 0.5],
      outline: {
        width: 1,
        color: '#FF0055',
        style: 'solid'
      }
    },
    visualVariables: [
    {
      type: 'size',
      field: 'temperature', // earthquake magnitude
      valueUnit: 'unknown',
      minDataValue: 10,
      maxDataValue: 40,
      // Define size of mag 2 quakes based on scale
      minSize: {
        type: 'size',
        valueExpression: '$view.temperature',
        stops: [
        {
          value: 1128,
          size: 5
        },
        {
          value: 36111,
          size: 4
        },
        {
          value: 9244649,
          size: 3
        },
        {
          value: 73957191,
          size: 2
        },
        {
          value: 591657528,
          size: 1
        }]
      },
      // Define size of mag 7 quakes based on scale
      maxSize: {
        type: 'size',
        valueExpression: '$view.temperature',
        stops: [
        {
          value: 1128,
          size: 20
        },
        {
          value: 36111,
          size: 30
        },
        {
          value: 9244649,
          size: 40
        },
        {
          value: 73957191,
          size: 50
        },
        {
          value: 591657528,
          size: 60
        }]
      }
    }]
  };

  get mapLoaded(): boolean {
    return this.paramIsloaded;
  }

  @Input()
  set zoom(zoom: number) {
    this.paramZoom = zoom;
  }

  get zoom(): number {
    return this.paramZoom;
  }

  @Input()
  set center(center: Array<number>) {
    this.paramCenter = center;
  }

  get center(): Array<number> {
    return this.paramCenter;
  }

  @Input()
  set basemap(basemap: string) {
    this.paramBasemap = basemap;
  }

  get basemap(): string {
    return this.paramBasemap;
  }

  @Input()
  set ground(ground: string) {
    this.paramGround = ground;
  }

  get ground(): string {
    return this.paramGround;
  }


  constructor() {
  }

  async initializeMap() {
    try {

      // 3D
      const [EsriMap, EsriMapView, EsriSceneView, EsriLocator, EsriExpand, EsriBasemapToggle,
        EsriBasemapGallery, EsriFeatureLayer, EsriNavigationToggle,
        EsriPoint, EsriPolygon, EsriLegend, EsriConfig, EsriRequest,
        EsriGraphicsLayer, EsriGraphic] = await loadModules([
        'esri/Map',
        'esri/views/MapView',
        'esri/views/SceneView',
        'esri/tasks/Locator',
        'esri/widgets/Expand',
        'esri/widgets/BasemapToggle',
        'esri/widgets/BasemapGallery',
        'esri/layers/FeatureLayer',
        'esri/widgets/NavigationToggle',
        'esri/geometry/Point',
        'esri/geometry/Polygon',
        'esri/widgets/Legend',
        'esri/config',
        'esri/request',
        'esri/layers/GraphicsLayer',
        'esri/Graphic'
      ]);

      this.EsriMap = EsriMap;
      this.EsriMapView = EsriMapView;
      this.EsriSceneView = EsriSceneView;
      this.EsriLocator = EsriLocator;
      this.EsriExpand = EsriExpand;
      this.EsriBasemapToggle = EsriBasemapToggle;
      this.EsriBasemapGallery = EsriBasemapGallery;
      this.EsriFeatureLayer = EsriFeatureLayer;
      this.EsriNavigationToggle = EsriNavigationToggle;
      this.EsriPoint = EsriPoint;
      this.EsriPolygon = EsriPolygon;
      this.EsriLegend = EsriLegend;
      this.EsriConfig = EsriConfig;
      this.EsriRequest = EsriRequest;
      this.EsriGraphicsLayer = EsriGraphicsLayer;
      this.EsriGraphic = EsriGraphic;

      // Configure the Map
      const mapProperties: esri.MapProperties = {
        basemap: this.paramBasemap,
        ground: this.paramGround
      };

      this.maps = new this.EsriMap(mapProperties);

      // Initialize the MapView
      const mapViewProperties: esri.MapViewProperties = {
        container: this.mapViewEl.nativeElement,
        center: this.paramCenter,
        zoom: this.paramZoom,
        map: this.maps,
        constraints: {
          snapToZoom: true
        },
        ui: {
          padding: {
            bottom: 15,
            right: 0
          }
        }
      };

      // 2D
      // this.mapView = new this.EsriMapView(mapViewProperties);

      // 3D
      this.mapView = new this.EsriSceneView(mapViewProperties);

      // locatortask
      this.locatorTask = new this.EsriLocator({
        url: 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer'
      });

      // Create the widget
      // const basemapToggle = new EsriBasemapToggle({
      //   view: mapView, // view that provides access to the map's 'topo' basemap
      //   nextBasemap: 'satellite' // allows for toggling to the 'hybrid' basemap
      // });

      const basemapGallery = new this.EsriBasemapGallery({
        view: this.mapView,
        source: {
          portal: {
            url: 'http://www.arcgis.com',
            useVectorBasemaps: false  // Load vector tile basemaps
          }
        }
      });

      const bgExpand = new this.EsriExpand({
        expandIconClass: 'esri-icon-collection',
        expandTooltip: 'Basemaps',
        view: this.mapView,
        content: basemapGallery
      });

      // close the expand whenever a basemap is selected
      basemapGallery.watch('activeBasemap', () => {
        const mobileSize = this.mapView.heightBreakpoint === 'xsmall' || this.mapView.widthBreakpoint === 'xsmall';
        if (mobileSize) {
          bgExpand.collapse();
        }
      });

      this.mapView.ui.add(bgExpand, 'top-right');

      this.mapView.ui.move([ 'zoom', 'navigation-toggle', 'compass' ], 'top-right');

      return this.mapView;

    } catch (error) {
      console.log('EsriLoader: ', error);
    }

  }



  // Finalize a few things once the MapView has been loaded
  houseKeeping(mapView) {
    this.mapView.when(() => {
      console.log('mapView ready: ', mapView.ready);
      this.paramIsloaded = mapView.ready;
      this.mapLoadedEvent.emit(true);

       //setTimeout(() => {
       //  this.setObject();
       //}, 3000);

       this.setPopUp();

       //this.setObject();
       //this.getData2();
      //this.basePolygon();
       this.baseLegend();
      this.getData();
        // .then(this.createGraphics) // then send it to the createGraphics() method
        // .then(this.createLayer) // when graphics are created, create the layer
        // .then(this.createLegend) // when layer is created, create the legend
        // .catch(this.errback);

    });


  }

  setPopUp() {

    this.mapView.popup.autoOpenEnabled = false;
    this.mapView.on('click', (event: any) => {
      // Get the coordinates of the click on the view
      // around the decimals to 3 decimals
      const lat = Math.round(event.mapPoint.latitude * 1000) / 1000;
      const lon = Math.round(event.mapPoint.longitude * 1000) / 1000;

      // Display the popup
      this.mapView.popup.open({
        title: 'Reverse geocode: [' + lon + ',' + lat + ']',
        location: event.mapPoint,
        content: ''
      });

      this.locatorTask.locationToAddress(event.mapPoint).then((response) => {
        this.mapView.popup.content = response.address;
      }).catch((error) => {
        this.mapView.popup.content =
          'No address was found for this location';
      });
    });
  }

  setObject() {
    console.log('setObject');
    const graphicsLayer = new this.EsriGraphicsLayer();

    // point
    const point = {
      type: 'point', // autocasts as new Point()
      x: 107.609,
      y: -6.919
    };

    const markerSymbol = {
      type: 'simple-marker', // autocasts as new SimpleMarkerSymbol()
      color: [226, 119, 40],
      outline: { // autocasts as new SimpleLineSymbol()
        color: [255, 255, 255],
        width: 2
      }
    };

    const pointGraphic = {
      type: 'Graphic',
      geometry: point,
      symbol: markerSymbol
    };

    graphicsLayer.add(pointGraphic);

    // polyline
    const polyline = {
      type: 'polyline', // autocasts as new Polyline()
      paths: [
        [107.593, -6.907],
        [107.629, -6.912]
      ]
    };

    const lineSymbol = {
      type: 'simple-line', // autocasts as SimpleLineSymbol()
      color: [226, 119, 40],
      width: 4
    };

    const polylineGraphic = {
      type: 'Graphic',
      geometry: polyline,
      symbol: lineSymbol
    };

    graphicsLayer.add(polylineGraphic);

    // polygon
    const polygon = {
      type: 'polygon', // autocasts as new Polygon()
      rings: [
        [107.594, -6.923],
        [107.615, -6.924],
        [107.611, -6.93],
        [107.599, -6.935],
        [107.573, -6.937]
      ]
    };

    const fillSymbol = {
      type: 'simple-fill', // autocasts as new SimpleFillSymbol()
      color: [227, 139, 79, 0.8],
      outline: { // autocasts as new SimpleLineSymbol()
        color: [255, 255, 255],
        width: 1
      }
    };

    const polygonGraphic = {
      type: 'Graphic',
      geometry: polygon,
      symbol: fillSymbol
    };

    graphicsLayer.add(polygonGraphic);

    console.log(graphicsLayer);

    this.maps.add(graphicsLayer);

  }

  basePolygon() {
    console.log('basePolygon');

    const graphicsLayer = new this.EsriGraphicsLayer();
    const url = 'http://localhost:4200/assets/data/kokab_v3.geojson';
    // this.EsriConfig.request.corsEnabledServers.push(url);
    this.EsriRequest(url, {responseType: 'json'}).then((geoJson) => {

      geoJson.data.features.map((feature, i) => {

        let varRings = [];
        if (feature.geometry.coordinates.length === 1) {
          varRings = feature.geometry.coordinates[0];
        } else {
          for (let j = 0; j < feature.geometry.coordinates.length; j++) {
            varRings[j] = feature.geometry.coordinates[j][0];
          }
        }

        const polygon = new this.EsriPolygon({
          hasM: true,
          rings: varRings,
          spatialReference: {wkid : 4326}
        });
        // console.log(polygon);

        const fillSymbol = {
          type: 'simple-fill', // autocasts as new SimpleFillSymbol()
          color: [227, 139, 79, 0.8],
          outline: { // autocasts as new SimpleLineSymbol()
            color: [255, 255, 255],
            width: 1
          }
        };

        const attribute = {
          ObjectID: i,
          id: feature.properties.id,
          kemendagri_kode: feature.properties.kemendagri_kode,
          kemendagri_nama: feature.properties.kemendagri_nama,
          bps_kode: feature.properties.bps_kode,
          bps_nama: feature.properties.bps_nama
        };

        const popupTemplates = {
          type: 'PopupTemplate',
          title: 'Kabupaten / Kota',
          content: [{
            type: 'fields',
            fieldInfos: [{
              fieldName: 'id',
              label: 'id',
              visible: true
            }, {
              fieldName: 'kemendagri_kode',
              label: 'Kemendagri Kode',
              visible: true
            }, {
              fieldName: 'kemendagri_nama',
              label: 'Kemendagri Nama',
              visible: true
            }, {
              fieldName: 'bps_kode',
              label: 'BPS Kode',
              visible: true
            }, {
              fieldName: 'bps_nama',
              label: 'bps_nama',
              visible: true
            }]
          }]
        };

        const polygonGraphic = {
          type: 'Graphic',
          geometry: polygon,
          symbol: fillSymbol,
          attributes: attribute,
          popupTemplate: popupTemplates
        };

        graphicsLayer.add(polygonGraphic);
      });

      this.maps.add(graphicsLayer);
      // this.legend = new this.EsriLegend({
      //   view: this.mapView,
      //   layerInfos: [
      //   {
      //     layer: graphicsLayer,
      //     title: 'Index kenyamanan tinggal'
      //   }]
      // });

      // this.mapView.ui.add(this.legend, 'bottom-right');
      // console.log(graphicsLayer);
    });
  }

  baseLegend() {
    console.log();
  }

  getData2() {
    console.log('getData2');

    const url = 'http://localhost:4200/assets/data/data1.json';
    // this.EsriConfig.request.corsEnabledServers.push(url);
    this.EsriRequest(url, {responseType: 'json'}).then((geoJson) => {

      const graphicsLayer = new this.EsriGraphicsLayer();

      geoJson.data.features.map((feature, i) => {

        // point
        const point = {
          type: 'point', // autocasts as new Point()
          x: feature.geometry.coordinates[0],
          y: feature.geometry.coordinates[1]
        };

        const markerSymbol = {
          type: 'simple-marker', // autocasts as new SimpleMarkerSymbol()
          color: [65, 113, 134],
          outline: { // autocasts as new SimpleLineSymbol()
            color: [255, 255, 255],
            width: 2
          }
        };

        const pointGraphic = {
          type: 'Graphic',
          geometry: point,
          symbol: markerSymbol
        };

        graphicsLayer.add(pointGraphic);
      });

      console.log(graphicsLayer);
      this.maps.add(graphicsLayer);
    });
  }

  getData() {
    console.log('getData');
    console.log('no param');

    const url = 'http://localhost:4200/assets/data/data1.json';
    // this.EsriConfig.request.corsEnabledServers.push(url);
    this.EsriRequest(url, {responseType: 'json'}).then((response) => {
      this.createGraphics(response);
    });
  }

  createGraphics(response) {
    console.log('createGraphics');
    console.log(response.data);

    const varQuakesRenderers = {
      type: 'simple-marker', // autocasts as new SimpleMarkerSymbol()
      style: 'circle',
      size: 20,
      color: [211, 255, 0, 0],
      outline: {
        width: 1,
        color: '#FF0055',
        style: 'solid'
      }
    };

    // raw GeoJSON data
    const geoJson = response.data;
    const varfeatures = [];
    // Create an array of Graphics from each GeoJSON feature
    geoJson.features.map((feature, i) => {
      let graphics: any;
      graphics = {
        type: 'Graphic',
        geometry: {
          type: 'point', // autocasts as new Point()
          x: feature.geometry.coordinates[0],
          y: feature.geometry.coordinates[1]
        },
        attributes: {
          ObjectID: i,
          id: feature.properties.id,
          index: feature.properties.index,
          temperature: feature.properties.temperature,
          place: feature.properties.place,
          time: feature.properties.time,
          url: feature.properties.url,
          types: feature.properties.types,
          type: feature.properties.type,
          title: feature.properties.title
        }
      };

      varfeatures.push(graphics);
    });
    // const graphicsLayer = new this.EsriGraphicsLayer();
    // graphicsLayer.add(graphics);
    // this.maps.add(graphicsLayer);

    this.createLayer(varfeatures);
  }

  createLayer(graphics) {
    console.log('createLayer');
    console.log(graphics);

    // console.log(this.varFields);
    // console.log(this.varQuakesRenderer);
    // console.log(this.varTemplate);

    this.layer = new this.EsriFeatureLayer({
      source: graphics, // autocast as an array of esri/Graphic
      fields: this.varFields, // This is required when creating a layer from Graphics
      objectIdField: 'ObjectID', // This must be defined when creating a layer from Graphics
      renderer: this.varQuakesRenderer, // set the visualization on the layer
      popupTemplate: this.varTemplate
    });
    // console.log(this.layer);

    // this.maps.add(this.layer);

    // this.legend = new this.EsriLegend({
    //   view: this.mapView,
    //   layerInfos: [
    //   {
    //     layer: this.layer,
    //     title: 'Earthquake'
    //   }]
    // });
    // this.mapView.ui.add(this.legend, 'bottom-right');
    this.createLegend(this.layer);
  }

  createLegend(layers) {
    console.log('createLegend');
    console.log(layers);

    // if the legend already exists, then update it with the new layer
    // if (this.legend) {
    //   this.legend.layerInfos = [{
    //     layer: layers,
    //     title: 'Magnitude'
    //   }];
    // } else {
    this.legend = new this.EsriLegend({
      view: this.mapView,
      layerInfos: [
      {
        layer: layers,
        title: 'Index kenyamanan tinggal'
      }]
    });

    this.legend.style = {
      type: 'classic',
      layout: 'auto'
    };

    const bgExpand = new this.EsriExpand({
      expandIconClass: 'esri-icon-description',
      expandTooltip: 'Info Legend',
      view: this.mapView,
      content: this.legend
    });

    this.maps.add(layers);
    this.mapView.ui.add(bgExpand, 'bottom-right');



    const legend2 = new this.EsriLegend({
      view: this.mapView,
      layerInfos: [
      {
        title: 'Index kenyamanan tinggal'
      }]
    });

    const bgExpand2 = new this.EsriExpand({
      expandIconClass: 'esri-icon-chart',
      expandTooltip: 'Info Chart',
      view: this.mapView,
      content: legend2
    });

    this.mapView.ui.add(bgExpand2, 'top-left');

    this.mapView.ui.padding = {top: 20, left: 20, right: 20, bottom: 40};

    // }
  }

  errback(error) {
    console.error('Creating legend failed. ', error);
  }

  ngOnInit() {
    // Initialize MapView and return an instance of MapView
    this.initializeMap().then((result) => {
      this.houseKeeping(result);
    });
  }

}
